#include <stdint.h>
#include <stdio.h>

#include "hw_regs.h"
#include <gram.h>
#include "dfii.h"
#include "helpers.h"
#include "io.h"
#include "console.h"

static void set_rdly(const struct gramCtx *ctx, unsigned int phase, unsigned int rdly) {
#ifdef GRAM_RW_FUNC
	if (phase == 0) {
		gram_write(ctx, (void*)&(ctx->phy->rdly_p0), rdly);
	} else if (phase == 1) {
		gram_write(ctx, (void*)&(ctx->phy->rdly_p1), rdly);
	}
#else
	if (phase == 0) {
		writel(rdly, (unsigned long)&(ctx->phy->rdly_p0));
	} else if (phase == 1) {
		writel(rdly, (unsigned long)&(ctx->phy->rdly_p1));
	}
#endif
}

void gram_reset_burstdet(const struct gramCtx *ctx) {
#ifdef GRAM_RW_FUNC
	gram_write(ctx, (void*)&(ctx->phy->burstdet), 0);
#else
	writel(0, (unsigned long)&(ctx->phy->burstdet));
#endif
}

bool gram_read_burstdet(const struct gramCtx *ctx, int phase) {
#ifdef GRAM_RW_FUNC
	return !!(gram_read(ctx, (void*)&(ctx->phy->burstdet)) & (1 << phase));
#else
	return !!(readl((unsigned long)&(ctx->phy->burstdet)) & (1 << phase));
#endif
}

int gram_generate_calibration(const struct gramCtx *ctx, struct gramProfile *profile) {
	unsigned char rdly;
	unsigned char min_rdly_p0, min_rdly_p1;
	unsigned char max_rdly_p0 = 7, max_rdly_p1 = 7;
	uint32_t tmp;
	volatile uint32_t *ram = ctx->ddr_base;
	size_t i;

	dfii_setsw(ctx, true);

	(void)tmp;

    puts("find min");
	// Find minimal rdly
	for (rdly = 0; rdly < 8; rdly++) {
		profile->rdly_p0 = rdly;
		gram_load_calibration(ctx, profile);
		gram_reset_burstdet(ctx);
        //puts("reset burst");

		for (i = 0; i < 128; i++) {
			tmp = readl((unsigned long)&(ram[i]));
		}

        //puts("read burst?");
		if (gram_read_burstdet(ctx, 0)) {
			min_rdly_p0 = rdly;
			break;
		} else if (rdly == 7) {
			return GRAM_ERR_RDLY_MAX;
		}
	}

	for (rdly = 0; rdly < 8; rdly++) {
		profile->rdly_p1 = rdly;
		gram_load_calibration(ctx, profile);
		gram_reset_burstdet(ctx);

		for (i = 0; i < 128; i++) {
			tmp = readl(&ram[i]);
		}

		if (gram_read_burstdet(ctx, 1)) {
			min_rdly_p1 = rdly;
			break;
		} else if (rdly == 7) {
			return GRAM_ERR_RDLY_MAX;
		}
	}

	// Find maximal rdly
	for (rdly = min_rdly_p0+1; rdly < 8; rdly++) {
		profile->rdly_p0 = rdly;
		gram_load_calibration(ctx, profile);
		gram_reset_burstdet(ctx);

		for (i = 0; i < 128; i++) {
			tmp = readl((unsigned long)&(ram[i]));
		}

		if (!gram_read_burstdet(ctx, 0)) {
			max_rdly_p0 = rdly - 1;
			break;
		} else if (rdly == 7) {
			return GRAM_ERR_RDLY_MAX;
		}
	}

	for (rdly = min_rdly_p1+1; rdly < 8; rdly++) {
		profile->rdly_p1 = rdly;
		gram_load_calibration(ctx, profile);
		gram_reset_burstdet(ctx);

		for (i = 0; i < 128; i++) {
			tmp = readl((unsigned long)&(ram[i]));
		}

		if (!gram_read_burstdet(ctx, 1)) {
			max_rdly_p1 = rdly - 1;
			break;
		} else if (rdly == 7) {
			return GRAM_ERR_RDLY_MAX;
		}
	}

	dfii_setsw(ctx, false);

	// Store average rdly value
	profile->rdly_p0 = (min_rdly_p0+max_rdly_p0)/2;
	profile->rdly_p1 = (min_rdly_p1+max_rdly_p1)/2;

	return GRAM_ERR_NONE;
}

void gram_load_calibration(const struct gramCtx *ctx, const struct gramProfile *profile) {
	dfii_setsw(ctx, true);
	set_rdly(ctx, 0, profile->rdly_p0);
	set_rdly(ctx, 1, profile->rdly_p1);
	dfii_setsw(ctx, false);
}
