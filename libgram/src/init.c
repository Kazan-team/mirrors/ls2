#include <gram.h>
#include "dfii.h"
#include "console.h"
#include "hw_regs.h"
#include "helpers.h"

extern void uart_writeuint32(uint32_t val);

int gram_init(struct gramCtx *ctx, const struct gramProfile *profile, void *ddr_base, void *core_base, void *phy_base) {
	ctx->ddr_base = ddr_base;
	ctx->core = core_base;
	ctx->phy = phy_base;

    //puts("phy base");
    //uart_writeuint32((unsigned long)&(ctx->phy));
    //puts("bdet");
    //uart_writeuint32((unsigned long)&(ctx->phy->burstdet));

	dfii_initseq(ctx, profile);
    puts("initseq\n");
	gram_load_calibration(ctx, profile);

    return GRAM_ERR_NONE;
}
