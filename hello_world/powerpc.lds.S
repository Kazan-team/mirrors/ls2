SECTIONS
{
	. = BOOT_INIT_BASE;
	_start = .;
	start = _start;
	. = BOOT_INIT_BASE;
	.head : {
		KEEP(*(.head))
 	}
	. = BOOT_INIT_BASE + 0x1000;
	.text : { *(.text) }
	. = BOOT_INIT_BASE + 0x1800;
	.data : { *(.data) }
	.bss : { *(.bss) }
}

