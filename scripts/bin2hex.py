#!/usr/bin/python3

import sys
import subprocess
import struct

if len(sys.argv) == 3:
    wordlen = int(sys.argv[2]) // 8
else:
    wordlen = 8

with open(sys.argv[1], "rb") as f:
    while True:
        word = f.read(wordlen)
        if len(word) == 8:
            print("%016x" % struct.unpack('<Q', word));
        elif len(word) == 4:
            if wordlen == 8:
                print("00000000%08x" % struct.unpack('<I', word));
            else:
                print("%08x" % struct.unpack('<I', word));
        elif len(word) == 0:
            exit(0);
        else:
            raise Exception("Bad length %d" % (len(word)))
