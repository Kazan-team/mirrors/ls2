/* This code is directly from Microwatt and is Copyright and Licensed
   under the same terms as Microwatt source code.
   https://github.com/antonblanchard/microwatt/blob/master/include/console.h
*/

#include <stddef.h>

void console_init(void);
void console_set_irq_en(bool rx_irq, bool tx_irq);
int getchar(void);
int putchar(int c);
int puts(const char *str);

#ifndef __USE_LIBC
size_t strlen(const char *s);
#endif
