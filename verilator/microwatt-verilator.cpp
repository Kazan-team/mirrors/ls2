// Copyright (C) IBM 2019, see LICENSE.CC4
// based on code from microwatt https://github.com/antonblanchard/microwatt

#include <stdlib.h>
#include "Vtop.h"
#include "verilated.h"
#include "verilated_vcd_c.h"

/*
 * Current simulation time
 * This is a 64-bit integer to reduce wrap over issues and
 * allow modulus.  You can also use a double, if you wish.
 */
vluint64_t main_time = 0;

/*
 * Called by $time in Verilog
 * converts to double, to match
 * what SystemC does
 */
double sc_time_stamp(void)
{
	return main_time;
}

#if VM_TRACE
VerilatedVcdC *tfp;
#endif

void tick(Vtop *top)
{
	top->clk = 1;
	top->eval();
#if VM_TRACE
	if (tfp)
		tfp->dump((double) main_time);
#endif
	main_time++;

	top->clk = 0;
	top->eval();
#if VM_TRACE
	if (tfp)
		tfp->dump((double) main_time);
#endif
	main_time++;
}

void uart_tx(unsigned char tx);
unsigned char uart_rx(void);

int main(int argc, char **argv)
{
	Verilated::commandArgs(argc, argv);

	// init top verilog instance
	Vtop* top = new Vtop;

#if VM_TRACE
	// init trace dump
	Verilated::traceEverOn(true);
	tfp = new VerilatedVcdC;
	top->trace(tfp, 99);
	tfp->open("microwatt-verilator.vcd");
#endif

	// Reset
	top->rst = 1;
	for (unsigned long i = 0; i < 5; i++)
		tick(top);
	top->rst = 0;

	while(!Verilated::gotFinish()) {
		tick(top);

		uart_tx(top->tx_o);
		top->rx_i = uart_rx();
	}

#if VM_TRACE
	tfp->close();
	delete tfp;
#endif

	delete top;
}
