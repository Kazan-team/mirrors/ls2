#include <stdint.h>
#include <stdbool.h>

#include "console.h"
#include "microwatt_soc.h"
#include "io.h"

#include <stdlib.h>
#include <stdint.h>
#include <gram.h>

#include "elf64.h"

#define ORANGECRAB_MODE_REGISTERS 0x0320, 0x0002, 0x0200, 0x0000

static inline void mtspr(int sprnum, unsigned long val)
{
    __asm__ volatile("mtspr %0,%1" : : "i" (sprnum), "r" (val));
}

static inline uint32_t read32(const void *addr)
{
	return *(volatile uint32_t *)addr;
}

static inline void write32(void *addr, uint32_t value)
{
	*(volatile uint32_t *)addr = value;
}

struct uart_regs {
	uint32_t divisor;
	uint32_t rx_data;
	uint32_t rx_rdy;
	uint32_t rx_err;
	uint32_t tx_data;
	uint32_t tx_rdy;
	uint32_t zero0; // reserved
	uint32_t zero1; // reserved
	uint32_t ev_status;
	uint32_t ev_pending;
	uint32_t ev_enable;
};

void uart_writeuint32(uint32_t val) {
	const char lut[] = { '0', '1', '2', '3', '4', '5', '6', '7',
                         '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
	uint8_t *val_arr = (uint8_t*)(&val);
	size_t i;

	for (i = 0; i < 4; i++) {
		putchar(lut[(val_arr[3-i] >> 4) & 0xF]);
		putchar(lut[val_arr[3-i] & 0xF]);
	}
}

void memcpy(void *dest, void *src, size_t n) {
    int i;
    //cast src and dest to char*
    char *src_char = (char *)src;
    char *dest_char = (char *)dest;
    for (i=0; i<n; i++) {
#if 1
        if ((i % 4096) == 0) {
            puts("memcpy ");
            uart_writeuint32(i);
            puts("\r\n");
        }
#endif
        dest_char[i] = src_char[i]; //copy contents byte by byte
    }
}

#if 0
void memcpy4(void *dest, void *src, size_t n) {
    int i;
    //cast src and dest to char*
    uint32_t *src_char = (uint32_t *)src;
    uint32_t *dest_char = (uint32_t *)dest;
    for (i=0; i<n/4; i++) {
#if 1
        if ((i % 4096) == 0) {
            puts("memcpy4 ");
            uart_writeuint32(i);
            puts("\r\n");
        }
#endif
        dest_char[i] = src_char[i]; //copy contents byte by byte
    }
}
#endif

void isr(void) {

}

extern void crank_up_qspi_level1(void);
extern int host_spi_flash_init(void);

static bool fl_read(void *dst, uint32_t offset, uint32_t size)
{
    uint8_t *d = dst;
    memcpy(d, (void *)(unsigned long)(SPI_FLASH_BASE + offset), size);
    return true;
}

static unsigned long copy_flash(unsigned int offset, unsigned int dst_offs)
{
    Elf64_Ehdr ehdr;
    Elf64_Phdr ph;
    unsigned int i, poff, size, off;
    void *addr;

    puts("Trying flash...\r\n");
    if (!fl_read(&ehdr, offset, sizeof(ehdr)))
        return -1ul;
    if (!IS_ELF(ehdr) || ehdr.e_ident[EI_CLASS] != ELFCLASS64) {
        puts("Doesn't look like an elf64\r\n");
        goto dump;
    }
    if (ehdr.e_ident[EI_DATA] != ELFDATA2LSB ||
        ehdr.e_machine != EM_PPC64) {
        puts("Not a ppc64le binary\r\n");
        goto dump;
    }

    poff = offset + ehdr.e_phoff;
    for (i = 0; i < ehdr.e_phnum; i++) {
        if (!fl_read(&ph, poff, sizeof(ph)))
            goto dump;
        if (ph.p_type != PT_LOAD)
            continue;

        /* XXX Add bound checking ! */
        size = ph.p_filesz;
        addr = (void *)ph.p_vaddr;
        off  = offset + ph.p_offset;
        //printf("Copy segment %d (0x%x bytes) to %p\n", i, size, addr);
        puts("Copy segment ");
        uart_writeuint32(i);
        puts(" size ");
        uart_writeuint32(size);
        puts(" addr ");
        uart_writeuint32((uint32_t)(unsigned long)addr);
        puts("\r\n");
        fl_read(addr+dst_offs, off, size);
        poff += ehdr.e_phentsize;
    }

    puts("Booting from DRAM at");
    uart_writeuint32((unsigned int)(dst_offs+ehdr.e_entry));
    puts("\r\n");

    puts("Dump DRAM\r\n");
    for (i = 0; i < 64; i++) {
        uart_writeuint32(readl(dst_offs+ehdr.e_entry+(i*4)));
        puts(" ");
        if ((i & 7) == 7) puts("\r\n");
    }
    puts("\r\n");

    //flush_cpu_icache();
    return dst_offs+ehdr.e_entry;
dump:
    puts("HDR: \r\n");
    for (i = 0; i < 8; i++) {
        uart_writeuint32(ehdr.e_ident[i]);
        puts("\r\n");
    }

    return -1ul;
}


// XXX
// Defining gram_[read|write] allows a trace of all register
// accesses to be dumped to console for debugging purposes.
// To use, define GRAM_RW_FUNC in gram.h
uint32_t gram_read(const struct gramCtx *ctx, void *addr) {
	uint32_t dword;

	puts("gram_read: ");
	uart_writeuint32((unsigned long)addr);
	dword = readl((unsigned long)addr);
	puts(": ");
	uart_writeuint32((unsigned long)dword);
	puts("\n");

	return dword;
}

int gram_write(const struct gramCtx *ctx, void *addr, uint32_t value) {
	puts("gram_write: ");
	uart_writeuint32((unsigned long)addr);
	puts(": ");
	uart_writeuint32((unsigned long)value);
	writel(value, (unsigned long)addr);
	puts("\n");

	return 0;
}

int main(void) {
	const int kNumIterations = 14;
	int res, failcnt = 0;
	uint32_t tmp;
    unsigned long ftr, spi_offs=0x0;
	volatile uint32_t *ram = (uint32_t*)MEMORY_BASE;

	console_init();
	//puts("Firmware launched...\n");

#if 1
    puts(" Soc signature: ");
    tmp = readl(SYSCON_BASE + SYS_REG_SIGNATURE);
    uart_writeuint32(tmp);
    tmp = readl(SYSCON_BASE + SYS_REG_SIGNATURE+4);
    uart_writeuint32(tmp);
    puts("  Soc features: ");
    ftr = readl(SYSCON_BASE + SYS_REG_INFO);
    if (ftr & SYS_REG_INFO_HAS_UART)
        puts("UART ");
    if (ftr & SYS_REG_INFO_HAS_DRAM)
        puts("DRAM ");
    if (ftr & SYS_REG_INFO_HAS_BRAM)
        puts("BRAM ");
    if (ftr & SYS_REG_INFO_HAS_SPI_FLASH)
        puts("SPIFLASH ");
    if (ftr & SYS_REG_INFO_HAS_LITEETH)
        puts("ETHERNET ");
    puts("\r\n");

    if (ftr & SYS_REG_INFO_HAS_SPI_FLASH) {
        puts("SPI Offset: ");
        spi_offs = readl(SYSCON_BASE + SYS_REG_SPI_INFO);
        uart_writeuint32(spi_offs);
        puts("\r\n");
    }

#endif

#if 1
#if 1
    if (ftr & SYS_REG_INFO_HAS_SPI_FLASH) {
        // print out configuration parameters for QSPI
        volatile uint32_t *qspi_cfg = (uint32_t*)SPI_FCTRL_BASE;
        for (int k=0; k < 2; k++) {
            tmp = readl((unsigned long)&(qspi_cfg[k]));
            puts("cfg");
            uart_writeuint32(k);
            puts(" ");
            uart_writeuint32(tmp);
            puts("\r\n");
        }
    }
#endif
    if (ftr & SYS_REG_INFO_HAS_SPI_FLASH) {
        volatile uint32_t *qspi = (uint32_t*)SPI_FLASH_BASE+0x900000;
        //volatile uint8_t *qspi_bytes = (uint8_t*)spi_offs;
         // let's not, eh? writel(0xDEAF0123, (unsigned long)&(qspi[0]));
         // tmp = readl((unsigned long)&(qspi[0]));
        for (int i=0;i<10;i++) {
          tmp = readl((unsigned long)&(qspi[i]));
          uart_writeuint32(tmp);
          puts(" ");
          if ((i & 0x7) == 0x7) puts("\r\n");
        }
        puts("\r\n");

        // speed up the QSPI to at least a sane level
        crank_up_qspi_level1();
        // run at saner level
        host_spi_flash_init();

        puts("SPI Offset: ");
        spi_offs = readl(SYSCON_BASE + SYS_REG_SPI_INFO);
        uart_writeuint32(spi_offs);
        puts("\r\n");

        /*
        for (i=0;i<256;i++) {
          tmp = readb((unsigned long)&(qspi_bytes[i]));
          uart_writeuint32(tmp);
          puts(" ");
        }
        */
#if 0
        while (1) {
            // quick read
            tmp = readl((unsigned long)&(qspi[0x1000/4]));
            puts("read 0x1000");
            uart_writeuint32(tmp);
            putchar(10);
        }
        while (1) {
            unsigned char c = getchar();
            putchar(c);
            if (c == 13) { // if CR send LF

                // quick read
                tmp = readl((unsigned long)&(qspi[1<<i]));
                puts("read ");
                uart_writeuint32(1<<i);
                puts(" ");
                uart_writeuint32(tmp);
                putchar(10);
                i++;
            }
        }

        return 0;
#endif
    }
#endif
#if 0
	volatile uint32_t *hyperram = (uint32_t*)0x00000000; // at 0x0 for arty
    writel(0xDEAF0123, (unsigned long)&(hyperram[0]));
    tmp = readl((unsigned long)&(hyperram[0]));
    int i = 0;
    while (1) {
        unsigned char c = getchar();
        putchar(c);
        if (c == 13) { // if CR send LF

            // quick write/read
            writel(0xDEAF0123+i, (unsigned long)&(hyperram[1<<i]));
            tmp = readl((unsigned long)&(hyperram[1<<i]));
            puts("read ");
            uart_writeuint32(1<<i);
            puts(" ");
            uart_writeuint32(tmp);
            putchar(10);
            i++;
        }
    }

    return 0;
#endif

    // init DRAM only if SYSCON says it exists (duh)
    if (ftr & SYS_REG_INFO_HAS_DRAM)
    {
        puts("DRAM init... ");

        struct gramCtx ctx;
#if 1
        struct gramProfile profile = {
            .mode_registers = {
                0xb20, 0x806, 0x200, 0x0
            },
            .rdly_p0 = 2,
            .rdly_p1 = 2,
        };
#endif
#if 0
        struct gramProfile profile = {
            .mode_registers = {
                0x0320, 0x0006, 0x0200, 0x0000
            },
            .rdly_p0 = 1,
            .rdly_p1 = 1,
        };
#endif
        struct gramProfile profile2;
        gram_init(&ctx, &profile, (void*)MEMORY_BASE,
                                  (void*)DRAM_CTRL_BASE,
                                  (void*)DRAM_INIT_BASE);
        puts("done\n");

        puts("MR profile: ");
        uart_writeuint32(profile.mode_registers[0]);
        puts(" ");
        uart_writeuint32(profile.mode_registers[1]);
        puts(" ");
        uart_writeuint32(profile.mode_registers[2]);
        puts(" ");
        uart_writeuint32(profile.mode_registers[3]);
        puts("\n");

        // FIXME
        // Early read test for WB access sim
        //uart_writeuint32(*ram);

#if 1
        puts("Rdly\np0: ");
        for (size_t i = 0; i < 8; i++) {
            profile2.rdly_p0 = i;
            gram_load_calibration(&ctx, &profile2);
            gram_reset_burstdet(&ctx);

            for (size_t j = 0; j < 128; j++) {
                tmp = readl((unsigned long)&(ram[i]));
            }
            if (gram_read_burstdet(&ctx, 0)) {
                puts("1");
            } else {
                puts("0");
            }
        }
        puts("\n");

        puts("Rdly\np1: ");
        for (size_t i = 0; i < 8; i++) {
            profile2.rdly_p1 = i;
            gram_load_calibration(&ctx, &profile2);
            gram_reset_burstdet(&ctx);
            for (size_t j = 0; j < 128; j++) {
                tmp = readl((unsigned long)&(ram[i]));
            }
            if (gram_read_burstdet(&ctx, 1)) {
                puts("1");
            } else {
                puts("0");
            }
        }
        puts("\n");

        puts("Auto calibrating... ");
        res = gram_generate_calibration(&ctx, &profile2);
        if (res != GRAM_ERR_NONE) {
            puts("failed\n");
            gram_load_calibration(&ctx, &profile);
        } else {
            gram_load_calibration(&ctx, &profile2);
        }
        puts("done\n");

        puts("Auto calibration profile:");
        puts("p0 rdly:");
        uart_writeuint32(profile2.rdly_p0);
        puts(" p1 rdly:");
        uart_writeuint32(profile2.rdly_p1);
        puts("\n");
#endif

        puts("Reloading built-in calibration profile...");
        gram_load_calibration(&ctx, &profile);

        puts("DRAM test... \n");
        for (size_t i = 0; i < kNumIterations; i++) {
            writel(0xDEAF0000 | i*4, (unsigned long)&(ram[i]));
        }

#if 0
        for (int dly = 0; dly < 8; dly++) {
            failcnt = 0;
            profile2.rdly_p0 = dly;
            profile2.rdly_p1 = dly;
            puts("p0 rdly:");
            uart_writeuint32(profile2.rdly_p0);
            puts(" p1 rdly:");
            uart_writeuint32(profile2.rdly_p1);
            gram_load_calibration(&ctx, &profile2);
            for (size_t i = 0; i < kNumIterations; i++) {
                if (readl((unsigned long)&(ram[i])) != (0xDEAF0000 | i*4)) {
                    puts("fail : *(0x");
                    uart_writeuint32((unsigned long)(&ram[i]));
                    puts(") = ");
                    uart_writeuint32(readl((unsigned long)&(ram[i])));
                    puts("\n");
                    failcnt++;

                    if (failcnt > 10) {
                        puts("Test canceled (more than 10 errors)\n");
                        break;
                    }
                }
            }
        }
#else
        failcnt = 0;
        for (size_t i = 0; i < kNumIterations; i++) {
            if (readl((unsigned long)&(ram[i])) != (0xDEAF0000 | i*4)) {
                puts("fail : *(0x");
                uart_writeuint32((unsigned long)(&ram[i]));
                puts(") = ");
                uart_writeuint32(readl((unsigned long)&(ram[i])));
                puts("\n");
                failcnt++;

                if (failcnt > 10) {
                    puts("Test canceled (more than 10 errors)\n");
                    break;
                }
            }
        }
    }
#endif
	puts("done\n");

#if 0 // ooo, annoying: won't work. no idea why
    // temporary hard-hack: boot directly from QSPI. really
    // should do something like detect at least... something
    if ((ftr & SYS_REG_INFO_HAS_SPI_FLASH))
    {
        // jump to absolute address
        mtspr(8, SPI_FLASH_BASE); // move address to LR
        __asm__ volatile("blr");
        return 0;
    }
#endif

    // memcpy from SPI Flash then boot
    if ((ftr & SYS_REG_INFO_HAS_SPI_FLASH) &&
        (failcnt == 0))
    {
/*
        puts("ELF @ QSPI\n");
        // identify ELF, copy if present, and get the start address
        unsigned long faddr = copy_flash(spi_offs,
                                         0x600000); // hack!
        if (faddr != -1ul) {
            // jump to absolute address
            mtspr(8, faddr); // move address to LR
            __asm__ volatile("blr");

            // works with head.S which copies r3 into ctr then does bctr
            return faddr;
        }
        puts("copy QSPI\n");
*/
        // another terrible hack: copy from flash at offset 0x600000
        // a block of size 0x600000 into mem address 0x600000, then
        // jump to it.  this allows a dtb image to be executed
        puts("copy QSPI\n");
        volatile uint32_t *mem = (uint32_t*)0x1000000;
        fl_read(mem,       // destination in RAM
                0x600000,  // offset into QSPI
                0x8000); // length - shorter (testing) 0x8000);
                //0x1000000); // length 
        puts("dump mem\n");
        for (int i=0;i<256;i++) {
          tmp = readl((unsigned long)&(mem[i]));
          uart_writeuint32(tmp);
          puts(" ");
          if ((i & 0x7) == 0x7) puts("\r\n");
        }
        puts("\r\n");
        mtspr(8, 0x1000000); // move address to LR
        __asm__ volatile("blr");
    }

	return 0;
}

