// Copyright (c) 2020 LambdaConcept <contact@lambdaconcept.com>
// Copyright (c) 2021 Luke Kenneth Casson Leighton <lkcl@lkcl.net>

`timescale 1 ns / 1 ns

module simsoc_hyperram_tb;
  // GSR & PUR init requires for Lattice models
  GSR GSR_INST (
    .GSR(1'b1)
  );
  PUR PUR_INST (
    .PUR (1'b1)
  );

  reg clkin;
  wire sync;
  wire sync2x;
  wire dramsync;
  wire init;

  // Generate 100 Mhz clock
  always 
    begin
      clkin = 1;
      #5;
      clkin = 0;
      #5;
    end

    // Outputs
    wire o_csn0;
    wire o_clk;
    wire o_resetn;

    // Bidirs
    wire [7:0] io_dq;
    wire io_rwds;

    // SPI
    wire spi_cs_n;
    wire spi_rst_n;
    wire [3:0] io_spi_dq;

s27kl0641
    #(
    .TimingModel("S27KL0641DABHI000"))
    hyperram (
    .DQ7(io_dq[7]),
    .DQ6(io_dq[6]),
    .DQ5(io_dq[5]),
    .DQ4(io_dq[4]),
    .DQ3(io_dq[3]),
    .DQ2(io_dq[2]),
    .DQ1(io_dq[1]),
    .DQ0(io_dq[0]),
    .RWDS(io_rwds),
    .CSNeg(o_csn0),
    .CK(o_clk),
    .RESETNeg(o_resetn)
    );

  // uart, LEDs, switches
  wire uart_tx ;
  reg uart_rx = 0;
  wire led_0;
  wire led_1;
  wire led_2;
  wire led_3;
  wire led_4;
  wire led_5;
  wire led_6;
  wire led_7;
  //reg switch_0 = 0;
  //reg switch_1 = 0;
  //reg switch_2 = 0;
  //reg switch_3 = 0;
  //reg switch_4 = 0;
  //reg switch_5 = 0;
  //reg switch_6 = 0;
  //reg switch_7 = 0;

  top simsoctop (
    // hyperram
    .hyperram_0__cs_n__io(o_csn0), 
    .hyperram_0__rst_n__io(o_resetn), 
    .hyperram_0__rwds__io(io_rwds), 
    .hyperram_0__ck__io(o_clk), 
    .hyperram_0__dq__io(io_dq),
    // Quad SPI
    //.spi_flash_4x_0__dq__io(io_spi_dq),
    //.spi_flash_4x_0__cs__io(spi_cs_n),
    .spi_0_0__dq0__io(io_spi_dq[0]),
    .spi_0_0__dq1__io(io_spi_dq[1]),
    .spi_0_0__dq2__io(io_spi_dq[2]),
    .spi_0_0__dq3__io(io_spi_dq[3]),
    .spi_0_0__cs_n__io(spi_cs_n),

    // uart
    .uart_0__rx__io(uart_rx),
    .uart_0__tx__io(uart_tx),
    // led
    .led_0__io(led_0),
    .led_1__io(led_1),
    .led_2__io(led_2),
    .led_3__io(led_3),
    .led_4__io(led_4),
    .led_5__io(led_5),
    .led_6__io(led_6),
    .led_7__io(led_7),
    // switches
    //.switch_0__io(switch_0),
    //.switch_1__io(switch_1),
    //.switch_2__io(switch_2),
    //.switch_3__io(switch_3),
    //.switch_4__io(switch_4),
    //.switch_5__io(switch_5),
    //.switch_6__io(switch_6),
    //.switch_7__io(switch_7),
    // clock/reset
    .clk100_0__p(clkin),
    .rst_0__io(1'b0)
  );

    cy15b104qs
    #(
    .mem_file_name("firmware.hex"))
     cy15b104qs
     (
      .CSNeg(spi_cs_n),
      .SCK(simsoctop.spi0.spi_clk),
      .RESETNeg(io_spi_dq[3]),
      .SI(io_spi_dq[0]),
      .SO(io_spi_dq[1]),
      .WPNeg(io_spi_dq[2])
      );

  initial
    begin
      $dumpfile("simsoc_hyperram.fst");
      $dumpvars(0, clkin);
      $dumpvars(0, o_resetn);
      $dumpvars(0, o_csn0);
      $dumpvars(0, o_clk);
      $dumpvars(0, io_rwds);
      $dumpvars(0, io_dq);
      $dumpvars(0, uart_tx);
      $dumpvars(0, uart_rx);
      $dumpvars(0, simsoctop);
      $dumpvars(0, cy15b104qs);
    end

  initial
    begin
      
      // run for a set time period then exit
      #120000000;

      $finish;
    end

endmodule
