// Copyright (c) 2020 LambdaConcept <contact@lambdaconcept.com>
// Copyright (c) 2021 Luke Kenneth Casson Leighton <lkcl@lkcl.net>

`timescale 1 ns / 1 ns

module simsoctb;
  // GSR & PUR init requires for Lattice models
  GSR GSR_INST (
    .GSR(1'b1)
  );
  PUR PUR_INST (
    .PUR (1'b1)
  );

  reg clkin;
  wire sync;
  wire sync2x;
  wire dramsync;
  wire init;

  // Generate 100 Mhz clock
  always 
    begin
      clkin = 1;
      #5;
      clkin = 0;
      #5;
    end

  // DDR3 init
  wire dram_ck;
  wire dram_cke;
  wire dram_we_n;
  wire dram_cs_n;
  wire dram_ras_n;
  wire dram_cas_n;
  wire [15:0] dram_dq;
  inout wire [1:0] dram_dqs;
  inout wire [1:0] dram_dqs_n;
  wire [13:0] dram_a;
  wire [2:0] dram_ba;
  wire [1:0] dram_dm;
  wire dram_odt;
  wire [1:0] dram_tdqs_n;
  wire dram_rst;

  ddr3 #(
    .check_strict_timing(0)
  ) ram_chip (
    .rst_n(dram_rst),
    .ck(dram_ck),
    .ck_n(~dram_ck),
    .cke(dram_cke),
    .cs_n(~dram_cs_n),
    .ras_n(dram_ras_n),
    .cas_n(dram_cas_n),
    .we_n(dram_we_n),
    .dm_tdqs(dram_dm),
    .ba(dram_ba),
    .addr(dram_a),
    .dq(dram_dq),
    .dqs(dram_dqs),
    .dqs_n(dram_dqs_n),
    .tdqs_n(dram_tdqs_n),
    .odt(dram_odt)
  );

  assign dram_dqs_n = (dram_dqs != 2'hz) ? ~dram_dqs : 2'hz;

  // uart, LEDs, switches
  wire uart_tx ;
  reg uart_rx = 0;
  wire led_0;
  wire led_1;
  wire led_2;
  wire led_3;
  wire led_4;
  wire led_5;
  wire led_6;
  wire led_7;
  reg switch_0 = 0;
  reg switch_1 = 0;
  reg switch_2 = 0;
  reg switch_3 = 0;
  reg switch_4 = 0;
  reg switch_5 = 0;
  reg switch_6 = 0;
  reg switch_7 = 0;

  //defparam ram_chip.
  
  top simsoctop (
     //FIXME .ddr3_0__rst__io(dram_rst),
    .ddr3_0__dq__io(dram_dq),
    .ddr3_0__dqs__p(dram_dqs),
    .ddr3_0__clk__p(dram_ck),
    .ddr3_0__clk_en__io(dram_cke),
    .ddr3_0__cs__io(dram_cs_n),
    .ddr3_0__we__io(dram_we_n),
    .ddr3_0__ras__io(dram_ras_n),
    .ddr3_0__cas__io(dram_cas_n),
    .ddr3_0__a__io(dram_a),
    .ddr3_0__ba__io(dram_ba),
    .ddr3_0__dm__io(dram_dm),
    .ddr3_0__odt__io(dram_odt),
    .uart_0__rx__io(uart_rx),
    .uart_0__tx__io(uart_tx),
    .led_0__io(led_0),
    .led_1__io(led_1),
    .led_2__io(led_2),
    .led_3__io(led_3),
    .led_4__io(led_4),
    .led_5__io(led_5),
    .led_6__io(led_6),
    .led_7__io(led_7),
    .switch_0__io(switch_0),
    .switch_1__io(switch_1),
    .switch_2__io(switch_2),
    .switch_3__io(switch_3),
    .switch_4__io(switch_4),
    .switch_5__io(switch_5),
    .switch_6__io(switch_6),
    .switch_7__io(switch_7),
    .clk100_0__p(clkin),
    .rst_0__io(1'b0)
  );

  initial
    begin
      $dumpfile("simsoc.fst");
      $dumpvars(0, clkin);
      $dumpvars(0, dram_rst);
      $dumpvars(0, dram_dq);
      $dumpvars(0, dram_dqs);
      $dumpvars(0, dram_ck);
      $dumpvars(0, dram_cke);
      $dumpvars(0, dram_cs_n);
      $dumpvars(0, dram_we_n);
      $dumpvars(0, dram_ras_n);
      $dumpvars(0, dram_cas_n);
      $dumpvars(0, dram_a);
      $dumpvars(0, dram_ba);
      $dumpvars(0, dram_dm);
      $dumpvars(0, dram_odt);
      $dumpvars(0, uart_tx);
      $dumpvars(0, uart_rx);
      $dumpvars(0, simsoctop);
      $dumpvars(0, ram_chip);
    end

  initial
    begin
      // run for a set time period then exit
      #120000000;

      $finish;
    end

endmodule
