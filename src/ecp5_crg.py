# Copyright (c) 2020 LambdaConcept <contact@lambdaconcept.com>
# Copyright (c) 2021 Luke Kenneth Casson Leighton <lkcl@lkcl.net>
# Copyright (c) 2018-2020 Florent Kermarrec <florent@enjoy-digital.fr>
# Copyright (c) 2019 Michael Betz <michibetz@gmail.com>
#
# Based on code from LambaConcept, from the gram example which is BSD-2-License
# https://github.com/jeanthom/gram/tree/master/examples
#
# Modifications for the Libre-SOC Project funded by NLnet and NGI POINTER
# under EU Grants 871528 and 957073, under the LGPLv3+ License


from nmigen import (Elaboratable, Module, Signal, ClockDomain, Instance,
                    ClockSignal, ResetSignal, Const)

__all__ = ["ECP5CRG"]


class PLL(Elaboratable):
    nclkouts_max    = 3
    clki_div_range  = (1, 128+1)
    clkfb_div_range = (1, 128+1)
    clko_div_range  = (1, 128+1)
    clki_freq_range = (    8e6,  400e6)
    clko_freq_range = (3.125e6,  400e6)
    vco_freq_range  = (  400e6,  800e6)

    def __init__(self, clkin,
                       clksel=Signal(shape=2, reset=2),
                       reset=Signal(reset_less=True),
                       locked=Signal()):
        self.clkin = clkin
        self.clkin_freq = None
        self.clksel = clksel
        self.locked = locked
        self.reset  = reset
        self.nclkouts   = 0
        self.clkouts    = {}
        self.config     = {}
        self.params     = {}

    def ports(self):
        return [
            self.clkin,
            self.clksel,
            self.lock,
        ] + list(self.clkouts.values())

    def set_clkin_freq(self, freq):
        (clki_freq_min, clki_freq_max) = self.clki_freq_range
        assert freq >= clki_freq_min
        assert freq <= clki_freq_max
        self.clkin_freq = freq

    def create_clkout(self, cd, freq, phase=0, margin=1e-2):
        (clko_freq_min, clko_freq_max) = self.clko_freq_range
        assert freq >= clko_freq_min
        assert freq <= clko_freq_max
        assert self.nclkouts < self.nclkouts_max
        self.clkouts[self.nclkouts] = (cd, freq, phase, margin)
        #create_clkout_log(self.logger, cd.name, freq, margin, self.nclkouts)
        print("clock domain", cd.domain, freq, margin, self.nclkouts)
        self.nclkouts += 1

    def compute_config(self):
        config = {}
        for clki_div in range(*self.clki_div_range):
            config["clki_div"] = clki_div
            for clkfb_div in range(*self.clkfb_div_range):
                all_valid = True
                vco_freq = self.clkin_freq/clki_div*clkfb_div*1 # clkos3_div=1
                (vco_freq_min, vco_freq_max) = self.vco_freq_range
                if vco_freq >= vco_freq_min and vco_freq <= vco_freq_max:
                    for n, (clk, f, p, m) in sorted(self.clkouts.items()):
                        valid = False
                        for d in range(*self.clko_div_range):
                            clk_freq = vco_freq/d
                            if abs(clk_freq - f) <= f*m:
                                config["clko{}_freq".format(n)]  = clk_freq
                                config["clko{}_div".format(n)]   = d
                                config["clko{}_phase".format(n)] = p
                                valid = True
                                break
                        if not valid:
                            all_valid = False
                else:
                    all_valid = False
                if all_valid:
                    config["vco"] = vco_freq
                    config["clkfb_div"] = clkfb_div
                    #compute_config_log(self.logger, config)
                    print ("PLL config", config)
                    return config
        raise ValueError("No PLL config found")

    def elaborate(self, platform):
        config = self.compute_config()
        clkfb = Signal()
        self.params.update(
            # attributes
            a_FREQUENCY_PIN_CLKI     = str(self.clkin_freq/1e6),
            a_ICP_CURRENT            = "6",
            a_LPF_RESISTOR           = "16",
            a_MFG_ENABLE_FILTEROPAMP = "1",
            a_MFG_GMCREF_SEL         = "2",
            # parameters
            p_FEEDBK_PATH   = "INT_OS3", # CLKOS3 rsvd for feedback with div=1.
            p_CLKOS3_ENABLE = "ENABLED",
            p_CLKOS3_DIV    = 1,
            p_CLKFB_DIV     = config["clkfb_div"],
            p_CLKI_DIV      = config["clki_div"],
            # reset, input clock, lock-achieved output
            i_RST           = self.reset,
            i_CLKI          = self.clkin,
            o_LOCK          = self.locked,
        )
        # for each clock-out, set additional parameters
        for n, (clk, f, p, m) in sorted(self.clkouts.items()):
            n_to_l = {0: "P", 1: "S", 2: "S2"}
            div    = config["clko{}_div".format(n)]
            cphase = int(p*(div + 1)/360 + div)
            self.params["p_CLKO{}_ENABLE".format(n_to_l[n])] = "ENABLED"
            self.params["p_CLKO{}_DIV".format(n_to_l[n])]    = div
            self.params["p_CLKO{}_FPHASE".format(n_to_l[n])] = 0
            self.params["p_CLKO{}_CPHASE".format(n_to_l[n])] = cphase
            self.params["o_CLKO{}".format(n_to_l[n])]        = clk

        m = Module()
        print ("params", self.params)
        pll = Instance("EHXPLLL", **self.params)
        m.submodules.pll = pll
        return m

        pll = Instance("EHXPLLL",
                       p_OUTDIVIDER_MUXA='DIVA',
                       p_OUTDIVIDER_MUXB='DIVB',
                       p_CLKOP_ENABLE='ENABLED',
                       p_CLKOS_ENABLE='ENABLED',
                       p_CLKOS2_ENABLE='DISABLED',
                       p_CLKOS3_ENABLE='DISABLED',
                       p_CLKOP_DIV=self.CLKOP_DIV,
                       p_CLKOS_DIV=self.CLKOS_DIV,
                       p_CLKFB_DIV=self.CLKFB_DIV,
                       p_CLKI_DIV=self.CLKI_DIV,
                       p_FEEDBK_PATH='INT_OP',
                       p_CLKOP_TRIM_POL="FALLING",
                       p_CLKOP_TRIM_DELAY=0,
                       p_CLKOS_TRIM_POL="FALLING",
                       p_CLKOS_TRIM_DELAY=0,
                       i_CLKI=self.clkin,
                       i_RST=0,
                       i_STDBY=0,
                       i_PHASESEL0=0,
                       i_PHASESEL1=0,
                       i_PHASEDIR=0,
                       i_PHASESTEP=0,
                       i_PHASELOADREG=0,
                       i_PLLWAKESYNC=0,
                       i_ENCLKOP=1,
                       i_ENCLKOS=1,
                       i_ENCLKOS2=0,
                       i_ENCLKOS3=0,
                       o_CLKOP=self.clkout1,
                       o_CLKOS=self.clkout2,
                       o_CLKOS2=self.clkout3,
                       o_CLKOS3=self.clkout4,
                       o_LOCK=self.lock,
                       )


class ECP5CRG(Elaboratable):
    def __init__(self, sys_clk_freq=100e6, dram_clk_freq=None,
                       pod_bits=25, sync_bits=26, need_bridge=False):
        """when dram_clk_freq=None, a dramsync domain is still created
        but it is an alias of sync domain.  likewise the 2x
        """
        self.sys_clk_freq = sys_clk_freq
        self.dram_clk_freq = dram_clk_freq
        self.pod_bits = pod_bits # for init domain
        self.sync_bits = sync_bits # for all other domains
        self.need_bridge = need_bridge # insert ECLKBRIDGECS
        assert pod_bits <= sync_bits, \
            "power-on-delay bits %d should " \
            " be less than sync_bits %d" % (pod_bits, sync_bits)

    def phase2_domain(self, m, pll, name, freq, esyncb):
        """creates a domain that can be used with xdr=4 platform resources.

        this requires creating a domain at *twice* the frequency,
        then halving it.  the doubled-frequency can then go to the
        eclk parts of a 4-phase IOPad:
            pads.a.o_clk.eq(ClockSignal("dramsync")),
            pads.a.o_fclk.eq(ClockSignal("dramsync2x")),
        """

        # create names
        cd2x = "%s2x" % name
        cd2x_ub = cd2x+"_unbuf"
        cd = name
        # Generating sync2x from extclk
        cd_2x = ClockDomain(cd2x, local=False)
        cd_2x_unbuf = ClockDomain(cd2x_ub, local=False, reset_less=True)
        cd_ = ClockDomain("%s" % name, local=False)

        # create PLL clocks
        pll.create_clkout(ClockSignal(cd2x_ub), 2*freq)
        if esyncb:
            if self.need_bridge:
                sys2x_clk_ecsout = Signal()
                m.submodules["%s_eclkbridgecs" % cd] = Instance("ECLKBRIDGECS",
                    i_CLK0 = ClockSignal(cd2x_ub),
                    i_SEL  = 0,
                    o_ECSOUT = sys2x_clk_ecsout)
                m.submodules["%s_eclksyncb" % cd] = Instance("ECLKSYNCB",
                    i_ECLKI = sys2x_clk_ecsout,
                    i_STOP  = 0,
                    o_ECLKO = ClockSignal(cd2x))
            else:
                m.submodules["%s_eclksyncb" % cd] = Instance("ECLKSYNCB",
                    i_ECLKI = ClockSignal(cd2x_ub),
                    i_STOP  = 0,
                    o_ECLKO = ClockSignal(cd2x))
        else:
            m.d.comb += ClockSignal(cd2x).eq(ClockSignal(cd2x_ub)) # no esyncb
        m.domains += cd_2x_unbuf
        m.domains += cd_2x
        m.domains += cd_

        # # Generating sync from sync2x
        m.submodules["%s_clkdivf" % cd] = Instance("CLKDIVF",
            p_DIV="2.0",
            i_ALIGNWD=0,
            i_CLKI=ClockSignal(cd2x),
            i_RST=0,
            o_CDIVX=ClockSignal(cd))

    def elaborate(self, platform):
        m = Module()

        # Get 100Mhz from oscillator
        extclk = platform.request(platform.default_clk)
        cd_rawclk = ClockDomain("rawclk", local=True, reset_less=True)
        m.d.comb += cd_rawclk.clk.eq(extclk)
        m.domains += cd_rawclk

        # Reset
        if platform.default_rst is not None:
            reset = platform.request(platform.default_rst).i
        else:
            reset = Const(0) # whoops

        gsr0 = Signal()
        gsr1 = Signal()

        m.submodules.gsr0 = Instance("FD1S3AX", p_GSR="DISABLED",
                                i_CK=ClockSignal("rawclk"),
                                i_D=~reset,
                                o_Q=gsr0)
        m.submodules.gsr1 = Instance("FD1S3AX", p_GSR="DISABLED",
                                i_CK=ClockSignal("rawclk"),
                                i_D=gsr0,
                                o_Q=gsr1)
        m.submodules.sgsr = Instance("SGSR", i_CLK=ClockSignal("rawclk"),
                             i_GSR=gsr1)

        # PLL
        m.submodules.pll = pll = PLL(ClockSignal("rawclk"), reset=~reset)

        # Power-on delay (655us)
        podcnt = Signal(self.pod_bits, reset=-1)
        synccnt = Signal(self.sync_bits, reset=-1)
        pod_done = Signal()
        sync_done = Signal()
        with m.If((synccnt != 0) & pll.locked):
            m.d.rawclk += synccnt.eq(synccnt-1)
        with m.If((podcnt != 0) & pll.locked):
            m.d.rawclk += podcnt.eq(podcnt-1)
        m.d.rawclk += pod_done.eq(podcnt == 0)
        m.d.rawclk += sync_done.eq(synccnt == 0)

        # and reset which only drops when the PLL is done and pod completes
        reset_ok = Signal(reset_less=True)
        sync_reset_ok = Signal(reset_less=True)
        m.d.comb += reset_ok.eq(~pll.locked|~pod_done)
        m.d.comb += sync_reset_ok.eq(~pll.locked|~sync_done)

        # create PLL input clock from platform default frequency
        pll.set_clkin_freq(platform.default_clk_frequency)

        # single or double main sync clock domain.  double needs a 2nd PLL
        # to match up with the CLKESYNCB, one per quadrant inside the ECP5
        if self.dram_clk_freq is not None:
            m.domains += ClockDomain("sync_unbuf", local=False, reset_less=True)
            m.domains += ClockDomain("sync", local=False)
            pll.create_clkout(ClockSignal("sync_unbuf"), self.sys_clk_freq)
            m.d.comb += ClockSignal("sync").eq(ClockSignal("sync_unbuf"))
        else:
            # Generating sync2x and sync from extclk, which is *only* how
            # xdr=4 can be requested on the sync domain. also do not request
            # an edge-clock-stop
            self.phase2_domain(m, pll, "sync", self.sys_clk_freq, True)
            m.d.comb += ResetSignal("sync2x").eq(sync_reset_ok)
        m.d.comb += ResetSignal("sync").eq(sync_reset_ok)

        # DRAM clock: if not requested set to sync, otherwise create with
        # a CLKESYNCB (which is set to no-stop at the moment)
        if self.dram_clk_freq is not None:
            self.phase2_domain(m, pll, "dramsync", self.dram_clk_freq, True)
        else:
            # alias dramsync and dramsync2x to sync and sync2x
            cd_dramsync = ClockDomain("dramsync", local=False)
            m.domains += cd_dramsync
            m.d.comb += ClockSignal("dramsync").eq(ClockSignal("sync"))
            # and a dram 2x sigh
            cd_dramsync2x = ClockDomain("dramsync2x", local=False)
            m.domains += cd_dramsync2x
            m.d.comb += ClockSignal("dramsync2x").eq(ClockSignal("sync2x"))
        # resets for the dram domains
        m.d.comb += ResetSignal("dramsync2x").eq(sync_reset_ok)
        m.d.comb += ResetSignal("dramsync").eq(sync_reset_ok)

        # create 25 mhz "init" clock, straight (no 2x phase stuff)
        # this domain can be used before all others, has its own delay
        # (sync_bits)
        cd_init = ClockDomain("init", local=False)
        pll.create_clkout(ClockSignal("init"), 25e6)
        m.domains += cd_init
        m.d.comb += ResetSignal("init").eq(reset_ok)

        return m

