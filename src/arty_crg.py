# Copyright (c) 2020 LambdaConcept <contact@lambdaconcept.com>
# Copyright (c) 2021 Luke Kenneth Casson Leighton <lkcl@lkcl.net>
# Copyright (c) 2018-2020 Florent Kermarrec <florent@enjoy-digital.fr>
# Copyright (c) 2019 Michael Betz <michibetz@gmail.com>
#
# Based on code from LambaConcept, from the gram example which is BSD-2-License
# https://github.com/jeanthom/gram/tree/master/examples
#
# Modifications for the Libre-SOC Project funded by NLnet and NGI POINTER
# under EU Grants 871528 and 957073, under the LGPLv3+ License

from nmigen import (Elaboratable, Module, Signal, ClockDomain, Instance,
                    ClockSignal, ResetSignal)
from nmigen.lib.cdc import ResetSynchronizer
import math

__ALL__ = ["ArtyCRG"]


# Helpers -----------------------------------------------------------------

def clkdiv_range(start, stop, step=1):
    start   = float(start)
    stop    = float(stop)
    step    = float(step)
    current = start
    while current < stop:
        yield int(current) if math.floor(current) == current else current
        current += step


# Xilinx / Generic -----------------------------------------------------

class XilinxClocking(Elaboratable):
    clkfbout_mult_frange = (2,  64+1)
    clkout_divide_range  = (1, 128+1)

    def __init__(self, clkin, vco_margin=0):
        self.clkin      = clkin
        self.vco_margin = vco_margin
        self.reset      = Signal(reset_less=True)
        self.locked     = Signal(reset_less=True)
        self.pod_done   = Signal(reset_less=True) # provide this @ rawclk
        self.clkin_freq = None
        self.vcxo_freq  = None
        self.nclkouts   = 0
        self.clkouts    = {}
        self.config     = {}
        self.params     = {}

    def set_clkin_freq(self, freq):
        self.clkin_freq = freq

    def create_clkout(self, cd, freq, phase=0, buf="bufg",
                            margin=1e-2, with_reset=True, ce=None):
        assert self.nclkouts < self.nclkouts_max
        clkout = Signal()
        self.clkouts[self.nclkouts] = (clkout, freq, phase, margin, cd,
                                       buf, with_reset, ce)

    def elaborate(self, platform):
        assert self.clkin_freq is not None
        m = Module()
        comb = m.d.comb
        for n, clkcfg in self.clkouts.items():
            (clkout, freq, phase, margin, cd, buf, with_reset, ce) = clkcfg
            if with_reset:
                arst = Signal()
                m.submodules += ResetSynchronizer(arst, domain=cd.name)
                comb += arst.eq(~self.locked | self.reset | ~self.pod_done)
            if buf is None:
                comb += cd.clk.eq(clkout)
            else:
                clkout_buf = Signal()
                comb += cd.clk.eq(clkout_buf)
                if buf == "bufg":
                    m.submodules += Instance("BUFG",
                                              i_I=clkout,
                                              o_O=clkout_buf)
                elif buf == "bufr":
                    m.submodules += Instance("BUFR",
                                              i_I=clkout,
                                              o_O=clkout_buf)
                elif buf == "bufgce":
                    if ce is None:
                        raise ValueError("BUFGCE requires user to provide "
                                         "a clock enable ce Signal")
                    m.submodules += Instance("BUFGCE",
                                              i_I=clkout,
                                              o_O=clkout_buf,
                                              i_CE=ce)
                elif buf == "bufio":
                    m.submodules += Instance("BUFIO",
                                              i_I=clkout,
                                              o_O=clkout_buf)
                else:
                    raise ValueError("Unsupported clock buffer: %s" % buf)
            print(cd.name, freq, margin, self.nclkouts)

        return m

    def compute_config(self):
        config = {}
        print ("compute_config", self.divclk_divide_range)
        print ("mult", self.clkfbout_mult_frange)
        print ("divrange", self.clkout_divide_range)
        for divclk_divide in range(*self.divclk_divide_range):
            config["divclk_divide"] = divclk_divide
            for clkfbout_mult in reversed(range(*self.clkfbout_mult_frange)):
                all_valid = True
                vco_freq = self.clkin_freq*clkfbout_mult/divclk_divide
                (vco_freq_min, vco_freq_max) = self.vco_freq_range
                if (vco_freq >= vco_freq_min*(1 + self.vco_margin) and
                    vco_freq <= vco_freq_max*(1 - self.vco_margin)):
                    for n, clkcfg in sorted(self.clkouts.items()):
                        (clkout, f, p, m, _, _, _, _) = clkcfg
                        valid = False
                        d_ranges = [self.clkout_divide_range]
                        r = getattr(self, "clkout%d_divide_range" % n, None)
                        if r is not None:
                            d_ranges += [r]
                        for d_range in d_ranges:
                            for d in clkdiv_range(*d_range):
                                clk_freq = vco_freq/d
                                if abs(clk_freq - f) <= f*m:
                                    config["clkout%d_freq" % n]   = clk_freq
                                    config["clkout%d_divide" % n] = d
                                    config["clkout%d_phase" % n]  = p
                                    valid = True
                                    break
                                if valid:
                                    break
                        if not valid:
                            all_valid = False
                else:
                    all_valid = False
                if all_valid:
                    config["vco"]           = vco_freq
                    config["clkfbout_mult"] = clkfbout_mult
                    print(config)
                    return config
        raise ValueError("No PLL config found")


# Xilinx / 7-Series --------------------------------------------------------

class S7PLL(XilinxClocking):
    nclkouts_max = 6
    clkin_freq_range = (19e6, 800e6)

    def __init__(self, clkin, speedgrade=-1):
        super().__init__(clkin)
        #self.logger = logging.getLogger("S7PLL")
        print ("Creating S7PLL, speedgrade %d" % speedgrade)
        self.divclk_divide_range = (1, 56+1)
        self.vco_freq_range = {
            -1: (800e6, 1600e6),
            -2: (800e6, 1866e6),
            -3: (800e6, 2133e6),
        }[speedgrade]

    def elaborate(self, platform):
        m = super().elaborate(platform)
        config = self.compute_config()
        pll_fb = Signal()
        self.params.update(
            p_STARTUP_WAIT="FALSE", o_LOCKED=self.locked, i_RST=self.reset,

            # VCO
            p_REF_JITTER1=0.01, p_CLKIN1_PERIOD=1e9/self.clkin_freq,
            p_CLKFBOUT_MULT=config["clkfbout_mult"],
            p_DIVCLK_DIVIDE=config["divclk_divide"],
            i_CLKIN1=self.clkin, i_CLKFBIN=pll_fb, o_CLKFBOUT=pll_fb,
        )
        for n, (clk, f, p, _, _, _, _, _) in sorted(self.clkouts.items()):
            div = config["clkout{}_divide".format(n)]
            phs = config["clkout{}_phase".format(n)]
            self.params["p_CLKOUT{}_DIVIDE".format(n)] = div
            self.params["p_CLKOUT{}_PHASE".format(n)] = phs
            self.params["o_CLKOUT{}".format(n)] = clk
        m.submodules += Instance("PLLE2_ADV", **self.params)
        return m


# CRG ----------------------------------------------------------------

class ArtyA7CRG(Elaboratable):
    def __init__(self, sys_clk_freq):
        self.sys_clk_freq = sys_clk_freq
        self.reset = Signal(reset_less=True)

    def elaborate(self, platform):
        m = Module()

        # reset
        reset = platform.request(platform.default_rst).i
        m.d.comb += self.reset.eq(reset)

        # Get 100Mhz from oscillator
        clk100 = platform.request("clk100")
        cd_rawclk = ClockDomain("rawclk", local=True, reset_less=True)
        m.d.comb += cd_rawclk.clk.eq(clk100)
        m.domains += cd_rawclk

        sync       = ClockDomain("sync")
        #sync2x     = ClockDomain("sync2x", reset_less=True)
        #sync4x     = ClockDomain("sync4x", reset_less=True)
        #sync4x_dqs = ClockDomain("sync4x_dqs", reset_less=True)
        #cd_clk200    = ClockDomain("cd_clk200")
        #cd_eth       = ClockDomain("cd_eth")
        dramsync      = ClockDomain("dramsync")

        m.domains += sync
        #m.domains += sync2x
        #m.domains += sync4x
        #m.domains += sync4x_dqs
        #m.domains += cd_clk200
        #m.domains += cd_eth
        m.domains += dramsync

        # PLL module
        m.submodules.pll = pll = S7PLL(clk100, speedgrade=-1)

        # Power-on delay (a LOT)
        podcnt = Signal(18, reset=-1)
        pod_done = Signal(reset_less=True)
        with m.If((podcnt != 0) & pll.locked):
            m.d.rawclk += podcnt.eq(podcnt-1)
        m.d.rawclk += pod_done.eq(podcnt == 0)

        # PLL setup
        m.d.comb += pll.reset.eq(reset)
        m.d.comb += pll.pod_done.eq(pod_done)
        pll.set_clkin_freq(100e6)
        pll.create_clkout(sync, self.sys_clk_freq)

        #platform.add_period_constraint(clk100_buf, 1e9/100e6)
        #platform.add_period_constraint(sync.clk, 1e9/sys_clk_freq)
        #platform.add_false_path_constraints(clk100_buf, sync.clk)

        # temporarily set dram sync clock exactly equal to main sync
        m.d.comb += ClockSignal("dramsync").eq(ClockSignal("sync"))

        return m


