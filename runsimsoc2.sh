#!/bin/bash
set -e

LIB_DIR=./src/ecp5u

# create the build_simsoc/top.il file with firmware baked-in
python3 src/ls2.py isim ./coldboot/coldboot.bin

# do some voodoo magic to get icarus to be happy with the ilang file
yosys simsoc.ys

# fix a bug in Lattice ECP5 models
cp ${LIB_DIR}/DDRDLLA.v DDRDLLA.v
patch DDRDLLA.v < DDRDLLA.patch

# string together the icarus verilog files and start runnin
iverilog -Wall -g2012 -s simsoctb -o simsoc \
        src/simsoctb.v ./top.v dram_model/ddr3.v \
    ${LIB_DIR}/ECLKSYNCB.v ${LIB_DIR}/EHXPLLL.v \
    ${LIB_DIR}/PUR.v ${LIB_DIR}/GSR.v \
	${LIB_DIR}/FD1S3AX.v ${LIB_DIR}/SGSR.v ${LIB_DIR}/ODDRX2F.v \
    ${LIB_DIR}/ODDRX2DQA.v ${LIB_DIR}/DELAYF.v ${LIB_DIR}/BB.v \
    ${LIB_DIR}/OB.v ${LIB_DIR}/IB.v \
	${LIB_DIR}/DQSBUFM.v ${LIB_DIR}/UDFDL5_UDP_X.v \
    ${LIB_DIR}/TSHX2DQSA.v ${LIB_DIR}/TSHX2DQA.v \
    ${LIB_DIR}/ODDRX2DQSB.v ${LIB_DIR}/IDDRX2DQA.v \
    ${LIB_DIR}/UDFDL5E_UDP_X.v \
    ${LIB_DIR}/OBZ.v \
    ${LIB_DIR}/DELAYG.v \
        ${LIB_DIR}/OFS1P3DX.v \
        ${LIB_DIR}/IFS1P3DX.v \
    DDRDLLA.v \
	${LIB_DIR}/CLKDIVF.v
vvp -n simsoc -fst-speed
