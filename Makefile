CFLAGS=-O3 -Wall
CXXFLAGS=-g -g

YOSYS     ?= yosys
NEXTPNR   ?= nextpnr-ecp5
ECPPACK   ?= ecppack
OPENOCD   ?= openocd

all = ls2_verilator

all: $(all)

uart_files = $(wildcard ../uart16550/rtl/verilog/*.v)

# Verilator sim
VERILATOR_ROOT=$(shell verilator -getenv VERILATOR_ROOT 2>/dev/null)
ifeq (, $(VERILATOR_ROOT))
$(soc_dram_tbs):
	$(error "Verilator is required to make this target !")
else

VERILATOR_CFLAGS=-O3
VERILATOR_FLAGS=-O3

endif

# Hello world
MEMORY_SIZE=8192
#RAM_INIT_FILE=hello_world/hello_world.bin
#RAM_INIT_FILE=../microwatt2/tests/xics/xics.bin
RAM_INIT_FILE=coldboot/coldboot.bin
SIM_MAIN_BRAM=false

# Micropython
#MEMORY_SIZE=393216
#RAM_INIT_FILE=micropython/firmware.bin

# Linux
#MEMORY_SIZE=536870912
#RAM_INIT_FILE=dtbImage.microwatt.bin
#SIM_MAIN_BRAM=false
SIM_BRAM_CHAINBOOT=6291456 # 0x600000

FPGA_TARGET ?= verilator

ifeq ($(FPGA_TARGET), verilator)
RESET_LOW=true
CLK_INPUT=100000000
CLK_FREQUENCY=100000000
clkgen=fpga/clk_gen_bypass.vhd
endif

ls2.v: src/ls2.py
	python3 src/ls2.py sim $(RAM_INIT_FILE)

# Need to investigate why yosys is hitting verilator warnings,
# and eventually turn on -Wall
microwatt-verilator: ls2.v \
                     verilator/microwatt-verilator.cpp \
                     verilator/uart-verilator.c
	verilator -O3 -CFLAGS "-DCLK_FREQUENCY=$(CLK_FREQUENCY) -I../verilator" \
	-DDATA_BUS_WIDTH_8 \
    --assert \
	--top-module top \
    --cc ls2.v \
    --cc external_core_top.v \
    --exe verilator/microwatt-verilator.cpp verilator/uart-verilator.c \
    -o $@ -I../uart16550/rtl/verilog \
	-Wno-fatal -Wno-CASEOVERLAP -Wno-UNOPTFLAT \
	    -Wno-BLKANDNBLK \
	    -Wno-COMBDLY  \
	    -Wno-CASEINCOMPLETE \
	    -Wno-WIDTH \
	    -Wno-TIMESCALEMOD \
	 --savable \
	 --trace \
	#    --unroll-count 256 \
	#    --output-split 5000 \
	#    --output-split-cfuncs 500 \
	#    --output-split-ctrace 500 \
	make -C obj_dir -f Vtop.mk
	@cp -f obj_dir/microwatt-verilator microwatt-verilator

clean:
	rm -fr obj_dir microwatt-verilator ls2.v

.PHONY: all clean
